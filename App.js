import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={styles.row}>
            <View style={styles.column}>
              <View style={styles.image}>
                <Text style={styles.radiusText}>Image</Text>
              </View>
            </View>
          </View>
        </View>

        <View style={styles.content}>
          <View style={styles.column}>
            <View style={styles.inputtex}>
              <Text style={styles.text}>INPUT TEXT</Text>
            </View>
            <View style={styles.inputtex}>
              <Text style={styles.text}>INPUT TEXT</Text>
            </View>
          </View>

          <View style={styles.column}>
            <View style={styles.inputtex}>
              <Text style={styles.text}>TouchableOpacity</Text>
              
            </View>
          </View>

        </View>


      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'red',
    flex: 1
  },
  header: {
    backgroundColor: 'blue',
    flex: 1
  },
  content: {
    backgroundColor: 'yellow',
    flex: 1
  },
  row: {                      
    backgroundColor: 'gray',
    flexDirection: 'row',
    alignItems: 'center',     
    flex: 1,
  },
  column: {                  
    backgroundColor: 'gray',
    flexDirection: 'column',
    alignItems: 'center',     
    flex: 1,
  },
  image: {
    backgroundColor: 'white',
    width: 200,
    height: 200,
    borderRadius: 100,
  },
  radiusText: {
    fontSize: 20,
    fontWeight: 'bold',
    padding: 70,
  },
  inputtex: {
    backgroundColor: 'white',
    flexDirection: 'column',
    alignItems: 'center',
    width: 300,
    height: 60,
    margin: 14,
  },
  text: {
    fontSize: 20,
    fontWeight: 'bold',
    padding: 10,
  },



})
export default App