import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

class App2 extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <View style={styles.head1}>
                        <Text style={styles.hamberg}></Text>
                        <Text style={styles.hamberg}></Text>
                        <Text style={styles.hamberg}></Text>

                    </View>
                    <View style={styles.head2}>
                        <View style={styles.center}>
                            <Text style={[styles.text, styles.center]}>Text</Text>
                        </View>
                    </View>
                    <View style={styles.head3}>
                        <Text style={styles.text}>X</Text>
                    </View>
                </View>

                <View style={styles.content}>

                    <Text style={styles.text}>ScrollView</Text>

                </View>

                <View style={styles.footer}>
                    <View style={styles.foot2}>
                        <Text style={styles.text}>I</Text>
                    </View>
                    <View style={styles.foot2}>
                        <Text style={styles.text}>C</Text>
                    </View>
                    <View style={styles.foot2}>
                        <Text style={styles.text}>O</Text>
                    </View>
                    <View style={styles.foot2}>
                        <Text style={styles.text}>N</Text>
                    </View>

                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'red',
        flex: 1
    },
    header: {
        backgroundColor: 'white',
        flexDirection: 'row',
        flex: 1
    },
    content: {
        backgroundColor: 'blue',
        flex: 10,
        justifyContent: 'center',
        alignItems: 'center',


    },
    footer: {
        backgroundColor: 'white',
        flexDirection: 'row',
        flex: 1
    },
    head1: {
        backgroundColor: 'green',
        flex: 1,
        margin: 2,
        marginLeft: -1,
        justifyContent: 'center',
        alignItems: 'center',

    },
    head2: {
        backgroundColor: 'green',
        flex: 3,
        margin: 2,

    },
    head3: {
        backgroundColor: 'green',
        flex: 1,
        margin: 2,
        marginRight: -1,
        alignItems: 'center',
    },
    text: {
        color: 'white',
        fontSize: 30,
        fontWeight: 'bold',
        padding: 10,
    },

    foot2: {
        backgroundColor: 'green',
        flex: 1,
        margin: 2,
        alignItems: 'center',

    },
    hamberg: {
        width: 35,
        height: 3.5,
        backgroundColor: 'white',
        margin: 2,
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center',
    },
})

export default App2